/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package lab.usermanagement;

import java.util.ArrayList;

/**
 *
 * @author peakk
 */
public class UserService {
    private ArrayList<User> userlist;
    private int lastid = 1;
    
    public UserService(){
        userlist = new ArrayList<User>();
    }
    
    public User addUser(User newUser){
        newUser.setId(lastid++);
        userlist.add(newUser);
        return newUser;
    }
    
    public User getuser(int index){
        return userlist.get(index); 
    }
    
    public ArrayList<User> getusers(){
        return userlist; 
    }
    
    public int getsize(){
        return userlist.size();
    }
    
    public void logUserlist(){
        for(User u: userlist){
            System.out.println(u);
        }
    }

    User updateUser(int index,  User updateUser) {
        User user = userlist.get(index);
        user.setLogin(updateUser.getLogin());
        user.setName(updateUser.getName());
        user.setPassword(updateUser.getPassword());
        user.setGender(updateUser.getGender());
        user.setRole(updateUser.getRole());
        return user;
        
        
    }

    User deleteUser(int index) {
        return userlist.remove(index);
        
    }
    
     
}