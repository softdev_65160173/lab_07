/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package lab.usermanagement;

import java.util.ArrayList;

/**
 *
 * @author peakk
 */
public class Usermanagement {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        User admin = new User("Admin","Administator","pass@1234",'M','A');
        User user1 = new User("User1","user1","pass@1234",'M','U');
        User user2 = new User("User2","user2","pass@1234",'F','U');
        User user3 = new User("User3","user3","pass@1234",'F','U');
        System.out.println("Admin");
        System.out.println("User1");
        System.out.println("User2");
         User[] userArr = new User[4];
         userArr[0] = admin;
         userArr[1] = user1;
         userArr[2] = user2;
         userArr[3] = user3;
         
         System.out.println("Print for userArr");
         for(int i=0;i<userArr.length;i++){
             System.out.println(userArr[i]);
         }
         
         ArrayList<User> userlist = new ArrayList<User >(); 
         System.out.println("ArrayList");
         userlist.add(admin);
         System.out.println(userlist.get(userlist.size()-1) +" List size "+ userlist.size());
         userlist.add(user1);
         System.out.println(userlist.get(userlist.size()-1) +" List size "+ userlist.size());
         userlist.add(user2);
         System.out.println(userlist.get(userlist.size()-1) +" List size "+ userlist.size());
          userlist.add(user3);
         System.out.println(userlist.get(userlist.size()-1) +" List size "+ userlist.size());
         for(int i=0;i<userlist.size();i++){
             System.out.println(userlist.get(i));
         }
         User user4 = new User("User4","user4","pass@1234",'M','U');
         userlist.set(0,user4);
         System.out.println(userlist.get(0));
         
    }
    
}
